package com.uwfcapstone.guidedtours.model;

import com.couchbase.lite.Document;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Model {

    public int id;
    public String name;
    public String description;

    public Model(){
        this.name = null;
        this.description = null;
        this.id = 0;
    }

    public Model(int id, String name,String description){
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public static <T> T ModelForDocument(Document document, Class<T> aClass){
        ObjectMapper m = new ObjectMapper();
        return m.convertValue(document.getProperties(), aClass);
    }

}
