package com.uwfcapstone.guidedtours.model;

public class Waypoint extends Model {

    //Stores the latitude of the waypoint
    public double latitude;
    //Stores the longitude of the waypoint
    public double longitude;

    public Waypoint(){
        //Initialize the id, name and description in the super class.
        super();
        //Initialize variables
        this.latitude = 0;
        this.longitude = 0;

    }

    public Waypoint(int id, String name, String description, double latitude, double longitude){

        //Initialize the id, name and description in the super class.
        super(id, name, description);
        //Store the latitude of the waypoint
        this.latitude = latitude;
        //Store the longitude of the waypoint
        this.longitude = longitude;

    }
}
