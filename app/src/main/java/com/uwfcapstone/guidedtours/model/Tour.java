package com.uwfcapstone.guidedtours.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Tour extends Model {

    public boolean isPublic;
    public int userID;
    public ArrayList<Waypoint> waypoints;
    private double averageLat;
    private double averageLong;

    public Tour(){

        //Construct the base object and initialize the id, name and description
        super();
        this.isPublic = false;
        this.userID = 0;
        this.waypoints = new ArrayList<Waypoint>(10);
        this.averageLat = 0;
        this.averageLat = 0;

        calculateAverageLatLong();

    }

    public Tour(int id, String name, String description, boolean publicTour, int userID, ArrayList<Waypoint> waypoints){

        //Construct the super model class that uses shared properties
        super(id, name, description);

        this.isPublic = publicTour;
        this.userID = userID;
        this.waypoints = waypoints;
        this.averageLat = 0;
        this.averageLat = 0;

        calculateAverageLatLong();


    }

    //Calculates the average latitude and longitude of an ArrayList of waypoints
    public void calculateAverageLatLong(){

        double latitude = 0.0;
        double longitude = 0.0;

        for (Waypoint waypoint : waypoints) {
            latitude += waypoint.latitude;
            longitude += waypoint.longitude;
        }

        this.averageLat = (latitude/waypoints.size());
        this.averageLong = (longitude/waypoints.size());

    }

    public double getAverageLatitude(){
        return averageLat;
    }

    public double getAverageLongitude(){
        return averageLong;
    }


}
