package com.uwfcapstone.guidedtours.model;


public class Error {

    public String name;
    public Class type;
    public String description;
    public String trace;
    public int lineNumber;
    public String fileName;

    public Error(){
        this.name = "Error Occurred";
        this.type = Error.class;
        this.description = "Unhandled error occurred";
        this.trace = null;
        this.lineNumber = 0;
        this.fileName = "Not set";

    }

    public Error(String name, Class type, String description){
        this.name = name;
        this.type = type;
        this.description = description;

    }
}
