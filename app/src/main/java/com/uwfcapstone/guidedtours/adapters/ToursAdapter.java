package com.uwfcapstone.guidedtours.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.uwfcapstone.guidedtours.R;
import com.uwfcapstone.guidedtours.model.Tour;

import java.util.ArrayList;

public class ToursAdapter extends ArrayAdapter<Tour> {

    private TextView tourName;
    private TextView tourDescription;

    public ToursAdapter(Context context, ArrayList<Tour> tours) {
        super(context, 0, tours);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Tour tour = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_tour, parent, false);
        }

        // Lookup view for data population
        tourName = (TextView) convertView.findViewById(R.id.tourName);
        tourDescription = (TextView) convertView.findViewById(R.id.tourDescription);

        // Populate the data into the template view using the data object
        tourName.setText(tour.name);
        tourDescription.setText(tour.description);

        // Return the completed view to render on screen
        return convertView;
    }

}