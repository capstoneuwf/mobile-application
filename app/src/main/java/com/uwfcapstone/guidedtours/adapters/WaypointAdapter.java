package com.uwfcapstone.guidedtours.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.uwfcapstone.guidedtours.R;
import com.uwfcapstone.guidedtours.model.Tour;
import com.uwfcapstone.guidedtours.model.Waypoint;

import java.util.ArrayList;

public class WaypointAdapter extends ArrayAdapter<Waypoint> {

    private TextView name;
    private TextView description;
    private TextView latitude;
    private TextView longitude;

    public WaypointAdapter(Context context, ArrayList<Waypoint> waypoints) {
        super(context, 0, waypoints);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Get the data item for this position
        Waypoint waypoint = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_waypoint, parent, false);
        }

        // Lookup view for data population
        name = (TextView) convertView.findViewById(R.id.waypoint_name);
        description = (TextView) convertView.findViewById(R.id.waypoint_description);
        latitude = (TextView) convertView.findViewById(R.id.waypoint_latitude);
        longitude = (TextView) convertView.findViewById(R.id.waypoint_longitude);

        name.setText(waypoint.name);
        description.setText(waypoint.description);
        latitude.setText(""+waypoint.latitude+"");
        longitude.setText(""+waypoint.longitude+"");

        // Return the completed view to render on screen
        return convertView;
    }

}