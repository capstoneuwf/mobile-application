package com.uwfcapstone.guidedtours;

import android.util.Log;

import com.couchbase.lite.Database;
import com.couchbase.lite.Manager;
import com.couchbase.lite.android.AndroidContext;

import android.content.Context;

public class DB {
    public static final String DB_NAME = "gpstourguide";
    public static final String TAG = "gpstourguide";

    public Manager manager = null;
    public Database database = null;

    public DB(Context ctx){

        try{
            manager = new Manager(new AndroidContext(ctx), Manager.DEFAULT_OPTIONS);
            database = manager.getDatabase(DB_NAME);
        } catch (Exception e){
            Log.d(TAG, "Error getting database", e);
        }

    }

}
