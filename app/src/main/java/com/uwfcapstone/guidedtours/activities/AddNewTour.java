package com.uwfcapstone.guidedtours.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.uwfcapstone.guidedtours.R;

public class AddNewTour extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_new_tour);
    }
}
