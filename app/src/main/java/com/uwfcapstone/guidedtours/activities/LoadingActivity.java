package com.uwfcapstone.guidedtours.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.uwfcapstone.guidedtours.R;
import com.uwfcapstone.guidedtours.api.ToursAPI;

import java.util.Timer;
import java.util.TimerTask;

public class LoadingActivity extends AppCompatActivity {

    public static Context ctx;
    private static int delay = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Get and set the application context globally within the context of this activity
        ctx = this.getApplicationContext();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                //Create the tours api object
                ToursAPI toursAPI = new ToursAPI(ctx);

                //Initialize the tours data on load
                toursAPI.initialize();

                //Start the dashboard activity
                Intent intent = new Intent(ctx, Navigation.class);
                startActivity(intent);

            }
        }, delay); //time in millis
    }

}
