package com.uwfcapstone.guidedtours.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.uwfcapstone.guidedtours.services.GPSTracker;
import com.uwfcapstone.guidedtours.R;
import com.uwfcapstone.guidedtours.adapters.ToursAdapter;
import com.uwfcapstone.guidedtours.api.ToursAPI;
import com.uwfcapstone.guidedtours.model.Tour;

import java.util.ArrayList;

public class Navigation extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ArrayAdapter<Tour> adapter;
    private ListView listView;
    private ArrayList<Tour> tours;
    private Context ctx;

    private Toolbar toolbar;
    private FloatingActionButton fab;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        ctx = this.getApplicationContext();

        // Construct the data source
        tours = (new ToursAPI(this.getApplicationContext())).getTours();

        Log.d("Tours data size", ""+tours.size()+"");

        Log.d("Tours data", tours.toString());

        initializeUIElements();

        getGPS();

    }

    public void getGPS(){

        GPSTracker gpsTracker = new GPSTracker(ctx);
        double latitude = gpsTracker.getLatitude();
        double longitude = gpsTracker.getLongitude();

        Log.d("Latitude", ""+latitude+"");
        Log.d("Longitude", ""+longitude+"");
    }

    public void initializeUIElements(){

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = (FloatingActionButton) findViewById(R.id.fab);

        // Attach the adapter to a ListView
        listView = (ListView) findViewById(R.id.TourListView);

        // Create the adapter to convert the array to views
        adapter = new ToursAdapter(this, tours);

        listView.setAdapter(adapter);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        navigationView = (NavigationView) findViewById(R.id.nav_view);

        setClickListeners();

    }

    public void setClickListeners(){

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ctx, AddNewTour.class);
                startActivity(intent);

            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {

                Intent i = new Intent(ctx, ReviewTour.class);
                i.putExtra("position", position);
                startActivity(i);
            }
        });

        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatementf
        if (id == R.id.action_settings) {
            //Take the user to the settings activity
            Intent intent = new Intent(this, SettingsActivity.class );
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.user_profile) {

            //Take the user to the login activity
            Intent intent = new Intent(this, LoginActivity.class );
            startActivity(intent);

        } else if(id == R.id.action_settings){
            //Take the user to the settings activity
            Intent intent = new Intent(this, SettingsActivity.class );
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
