package com.uwfcapstone.guidedtours.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.uwfcapstone.guidedtours.api.ToursAPI;
import com.uwfcapstone.guidedtours.model.Tour;
import com.uwfcapstone.guidedtours.model.Waypoint;
import com.uwfcapstone.guidedtours.R;

import java.util.ArrayList;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ArrayList<Tour> tours;
    private Tour tour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        int position = getIntent().getIntExtra("position", 0);

        //Create a new object of the ToursAPI class.
        ToursAPI toursAPI = new ToursAPI(this.getApplicationContext());

        this.tours = toursAPI.getTours();

        this.tour = this.tours.get(position);

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        // Setting UI Controls
        UiSettings mapSettings;
        mapSettings = mMap.getUiSettings();
        mapSettings.setZoomControlsEnabled(true);

        try {

            Log.d("Debugging tours data", this.tours.toString());

            if(this.tours.size() > 0 ){

                //Iterate over the waypoints of tour 1 and display them on the map
                for (Waypoint waypoint: this.tour.waypoints) {

                    LatLng wp = new LatLng(waypoint.latitude, waypoint.longitude);

                    mMap.addMarker(new MarkerOptions()
                            .position(wp)
                            .title(waypoint.name)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                            .snippet(waypoint.description));

                    mMap.moveCamera(CameraUpdateFactory.newLatLng(wp));

                    mMap.animateCamera( CameraUpdateFactory.zoomTo(14.0f));

                }

                // Set the map Type
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

            }

        } catch(Exception e){
            Log.d("Maps Error", e.getMessage());
        }




    }
}
