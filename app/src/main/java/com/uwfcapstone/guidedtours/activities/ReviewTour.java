package com.uwfcapstone.guidedtours.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.uwfcapstone.guidedtours.R;
import com.uwfcapstone.guidedtours.adapters.WaypointAdapter;
import com.uwfcapstone.guidedtours.api.ToursAPI;
import com.uwfcapstone.guidedtours.model.Tour;
import com.uwfcapstone.guidedtours.model.Waypoint;

import java.util.ArrayList;

public class ReviewTour extends AppCompatActivity {

    private EditText tour_name;
    private EditText tour_description;
    private ListView waypoints_list_view;
    private Button start_tour;

    private ArrayList<Tour> tours;
    private Tour tour;

    private Context ctx;

    private ArrayAdapter<Waypoint> waypointArrayAdapter;

    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);

        ctx = this.getApplicationContext();

        LoadData();

        SetupUI();

        SetupClickListeners();

    }

    public void LoadData(){

        position = getIntent().getIntExtra("position", 0);

        //Create a new object of the ToursAPI class.
        ToursAPI toursAPI = new ToursAPI(this.getApplicationContext());

        this.tours = toursAPI.getTours();

        this.tour = this.tours.get(position);

    }

    public void SetupUI(){

        setContentView(R.layout.activity_review_tour);

        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        tour_name = (EditText) findViewById(R.id.review_tour_name);

        if(tour.name != null){
            tour_name.setText(tour.name);
        }

        tour_description = (EditText) findViewById(R.id.review_tour_description);

        if(tour.description != null){
            tour_description.setText(tour.description);
        }

        waypoints_list_view = (ListView) findViewById(R.id.waypoints_list_view);

        start_tour = (Button) findViewById(R.id.start_tour);

        waypointArrayAdapter = new WaypointAdapter(ctx, this.tour.waypoints);

        waypoints_list_view.setAdapter(waypointArrayAdapter);
    }

    public void SetupClickListeners(){

        start_tour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ctx, MapsActivity.class);
                i.putExtra("position", position);
                startActivity(i);
            }
        });

    }
}
