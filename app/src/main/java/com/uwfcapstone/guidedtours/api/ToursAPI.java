package com.uwfcapstone.guidedtours.api;

import android.content.Context;
import android.util.Log;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Document;
import com.couchbase.lite.UnsavedRevision;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uwfcapstone.guidedtours.model.Error;
import com.uwfcapstone.guidedtours.model.Tour;
import com.uwfcapstone.guidedtours.model.Waypoint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ToursAPI extends API {

    //Store Error Object
    public Error error;

    //Store an ArrayList of Tours
    public ArrayList<Tour> tours;

    //Stores the document name
    public String documentName = "tours";

    //The Couchbase Document being interacted with
    public Document document;

    public ToursAPI(Context ctx){

        //Set up database related settings
        super(ctx);

        //Create the couchbase document
        this.document = this.database.getDocument(documentName);

    }

    public void initialize(){

        //The document needs to be initialized with default data
        if(this.document.getProperties() == null){

            //Tour 1 Related Setup

            //Create an ArrayList to store the waypoints for tour1
            ArrayList<Waypoint> waypoints_tour1 = new ArrayList<Waypoint>();



            //Create the waypoints
            Waypoint wp1_tour1 = new Waypoint(0, "Tennis Courts", "It's a great day to play some tennis!", 30.547285, -87.219529 );
            Waypoint wp2_tour1 = new Waypoint(1, "UWF Library", "Want to read a book? The library is the place for you!", 30.5499943, -87.2172661);
            Waypoint wp3_tour1 = new Waypoint(2, "Engineering Building", "Let's build a robot!" ,30.5469021, -87.2165138);
            Waypoint wp4_tour1 = new Waypoint(3, "Cannon Green", "A great place to meet and greet!", 30.5494701, -87.2176414 );
            Waypoint wp5_tour1 = new Waypoint(4, "Argo Galley", "Where you can grab a bite to eat!", 30.5490535, -87.2187173);

            //Add the waypoints to the waypoints ArrayList
            waypoints_tour1.add(wp1_tour1);
            waypoints_tour1.add(wp2_tour1);
            waypoints_tour1.add(wp3_tour1);
            waypoints_tour1.add(wp4_tour1);
            waypoints_tour1.add(wp5_tour1);


            //Create the first tool and add the waypoints
            Tour tour1 = new Tour(0, "UWF Campus Tour", "Tour the University of West Florida Campus", true, 1, waypoints_tour1);


            //Create an ArrayList to store the waypoints for tour2
            ArrayList<Waypoint> waypoints_tour2 = new ArrayList<>();

            waypoints_tour2.add(new Waypoint(0, "Fort Pickens", "Part of Gulf Islands National Seashore", 30.3282109, -87.2947568));
            waypoints_tour2.add(new Waypoint(1, "Pensacola Beach Ball", "Pensacola Beach Watertower", 30.4093876, -87.2366633));
            waypoints_tour2.add(new Waypoint(2, "Pensacola Bayfront Stadium", "Home of the Pensacola Blue Wahoos", 30.4044787, -87.2207843  ));
            waypoints_tour2.add(new Waypoint(3, "McGuire's Irish Pub", "Come enjoy a hearty burger and infamous Irish Wake!", 30.4180611, -87.204658 ));
            waypoints_tour2.add(new Waypoint(4, "Pensacola Musuem of Art", "Enjoy a seasonal variety of local art!", 30.4078976, -87.2160517));


            Tour tour2 = new Tour(1, "Pensacola Tour", "Tour Historic Pensacola", true, 1, waypoints_tour2);

            this.addTour(tour1);
            this.addTour(tour2);

        }

    }

    public void addTour(Tour tour){

        //Create a hashmap to store the arraylist of tours in the document
        Map<String, Object> properties = new HashMap<String, Object>();

        //No tours exists in the document
        if(this.document.getProperties() == null){

            //Create an ArrayList of Tours to store
            this.tours = new ArrayList<Tour>();

            //Add the new tour
            this.tours.add(tour);

            try {
                //Insert the tours into the properties key value map
                properties.put(this.documentName, this.tours);

                //Update the document with the new properties
                this.document.putProperties(properties);

            } catch (CouchbaseLiteException e){

                Log.d("Couchbase error", e.getMessage());

            } catch(Exception e) {

                Log.d("Exception occurred", e.getMessage());
            }

           //Tours already exist, have to modify the existing document
        } else {

            ObjectMapper objectMapper = new ObjectMapper();

            //Retrieve the existing tours from the document
            this.tours = objectMapper.convertValue(this.document.getProperties().get("tours"), new TypeReference<ArrayList<Tour>>(){});

            //Check to see if this tour id already exists
            boolean match = false;

            //Stores the last stored tour id
            int lastID = 0;

            //Iterate over the tours
            for (Tour currentTour : this.tours) {

                //If there is a tour name match, update the tour with the new data.
                    if(currentTour.name.equalsIgnoreCase(tour.name)){
                        match = true;
                        //Update the tour
                        this.tours.set(tours.indexOf(currentTour), tour);
                    }

                    //If the currentTourID is the greatest in the list update the lastID variable
                    if(currentTour.id > lastID){
                        lastID = currentTour.id;
                    }
                }

                //There wasn't a match
                if(!match){
                    //Set the tour id to be 1 greater than the last in the list
                    tour.id = lastID + 1;
                    //Add the new tour
                    this.tours.add(tour);
                }

            try {
                Document doc = database.getDocument(this.documentName);

                doc.update(new Document.DocumentUpdater() {
                    @Override
                    public boolean update(UnsavedRevision newRevision) {
                        Map<String, Object> properties = newRevision.getProperties();
                        properties.put("tours", tours);
                        newRevision.setProperties(properties);
                        return true;
                    }
                });

            } catch (CouchbaseLiteException e){

                Log.d("Couchbase error", e.getMessage());

            } catch(Exception e) {

                Log.d("Exception occurred", e.getMessage());
            }


        }



    }

    public boolean updateTour(){

        return true;
    }

    public ArrayList<Tour> getTours(){

        ObjectMapper objectMapper = new ObjectMapper();

        // Create the document and initialize the document with data
        if( this.document.getProperties() == null){

            return (new ArrayList<Tour>());

        } else {

            this.tours = objectMapper.convertValue(this.document.getProperties().get("tours"), new TypeReference<ArrayList<Tour>>(){});

            return this.tours;

        }

    }


    public boolean deleteTour(int id) {

        return true;

    }

}
