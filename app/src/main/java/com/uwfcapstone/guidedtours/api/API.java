package com.uwfcapstone.guidedtours.api;

import android.content.Context;

import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.Manager;
import com.uwfcapstone.guidedtours.DB;

public class API {

    final String TAG = "GPS Tour Guide";

    //The Couchbase database
    public Database database = null;

    //The Couchbase Manager
    public Manager manager = null;

    //The Android Application Context
    public Context ctx;




    public API(Context context){

        //Creating a new database wrapper
        DB db = new DB(context);

        //Get the couchbase database
        this.database = db.database;

        //Get the couchbase database manager
        this.manager = db.manager;

        //Set the Android Application Context
        this.ctx = context;

    }

    public Document create(){

        return database.createDocument();
    }

    public Document get(String docID){

        return database.getDocument(docID);
    }

    public boolean update(){

        return true;
    }

    public boolean delete(){

        return true;
    }

}
